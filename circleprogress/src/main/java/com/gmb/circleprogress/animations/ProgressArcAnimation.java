package com.gmb.circleprogress.animations;

import android.animation.ValueAnimator;
import android.view.animation.LinearInterpolator;


public class ProgressArcAnimation implements ArcAnimation  {

    private ValueAnimator progressAnim;

    ProgressArcAnimation(ValueAnimator.AnimatorUpdateListener updateListener) {

        progressAnim = new ValueAnimator(); //.ofFloat(0f, 360f);
        progressAnim.setInterpolator(new LinearInterpolator());
        progressAnim.setDuration(ArcAnimationFactory.PROGRESS_ANIMATOR_DURATION);
        progressAnim.addUpdateListener(updateListener);
    }


    @Override
    public ValueAnimator getAnimator() {
        return progressAnim;
    }
}
