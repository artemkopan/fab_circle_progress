package com.gmb.circleprogress;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import com.gmb.circleprogress.animations.AnimationUtils;
import com.gmb.circleprogress.animations.ArcAnimationFactory;


public class CircleProgress extends View {

    private final RectF arcBounds = new RectF();

    private ProgressCompleteListener progressCompleteListener;

    private float currentSweepAngle;
    private float currentRotationAngleOffset;
    private float currentRotationAngle;

    private ArcAnimationFactory animationFactory;
    private ValueAnimator progressAnim;
    private ValueAnimator rotateAnim;
    private ValueAnimator growAnim;
    private ValueAnimator shrinkAnim;
    private ValueAnimator completeAnim;
    private ObjectAnimator fadeOutAnim;
    private ObjectAnimator fadeInAnim;

    private boolean growing;
    private boolean completeAnimOnNextCycle;

    private int minSweepAngle;
    private int maxSweepAngle;

    private Paint paint;

    private float arcWidth;
    private int arcColor;
    private int maxProgress = 100;

    public CircleProgress(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(attrs);

    }

    private void init(AttributeSet attrs) {
        setupInitialAttributes(attrs);
        initPaint(true);
        setupAnimations();
    }

    private void setupInitialAttributes(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray attrArray = getContext().obtainStyledAttributes(attrs, R.styleable.CircleProgress, 0, 0);
            try {
                arcColor = attrArray.getColor(R.styleable.CircleProgress_arcColor, ContextCompat.getColor(getContext(), R.color.orange));
                arcWidth = attrArray.getDimensionPixelSize(R.styleable.CircleProgress_arcWidth, getResources().getDimensionPixelSize(R.dimen.progress_arc_stroke_width));
            } finally {
                attrArray.recycle();
            }
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        setSize(w, h);
    }

    public void setSize(int w, int h) {
        arcBounds.set(arcWidth, arcWidth, w - arcWidth, h - arcWidth);
    }

    private void initPaint(boolean roundedStroke) {
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(arcWidth);
        paint.setStrokeCap(roundedStroke ? Paint.Cap.ROUND : Paint.Cap.BUTT);
        paint.setColor(arcColor);
    }

    private void setupAnimations() {
        animationFactory = new ArcAnimationFactory();
        minSweepAngle = ArcAnimationFactory.MINIMUM_SWEEP_ANGLE;
        maxSweepAngle = ArcAnimationFactory.MAXIMUM_SWEEP_ANGLE;

        setupRotateAnimation();
        setupGrowAnimation();
        setupShrinkAnimation();
        setupCompleteAnimation();
        setupProgressAnimation();
        setupHideAnim();
        setupShowAnim();
    }

    private void setupProgressAnimation() {
        progressAnim = animationFactory.buildAnimation(ArcAnimationFactory.Type.PROGRESS,
                new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float animatedFraction = AnimationUtils.getAnimateAngle(animation);
                        updateCurrentSweepAngle(animatedFraction);
                    }
                }, null);
    }


    private void setupRotateAnimation() {
        rotateAnim = animationFactory.buildAnimation(ArcAnimationFactory.Type.ROTATE,
                new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float angle = AnimationUtils.getAnimatedFraction(animation) * 360f;
                        updateCurrentRotationAngle(angle);
                    }
                }, null);
    }

    private void setupGrowAnimation() {
        growAnim = animationFactory.buildAnimation(ArcAnimationFactory.Type.GROW,
                new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float animatedFraction = AnimationUtils.getAnimatedFraction(animation);
                        float angle = minSweepAngle + animatedFraction * (maxSweepAngle - minSweepAngle);
                        updateCurrentSweepAngle(angle);
                    }
                }, new Animator.AnimatorListener() {
                    boolean cancelled = false;

                    @Override
                    public void onAnimationStart(Animator animation) {
                        cancelled = false;
                        growing = true;
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (!cancelled) {
                            setShrinking();
                            shrinkAnim.start();
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        cancelled = true;
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                    }
                });
    }

    private void setupShrinkAnimation() {
        shrinkAnim = animationFactory.buildAnimation(ArcAnimationFactory.Type.SHRINK,
                new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float animatedFraction = AnimationUtils.getAnimatedFraction(animation);
                        updateCurrentSweepAngle(
                                maxSweepAngle - animatedFraction * (maxSweepAngle - minSweepAngle));
                    }
                }, new Animator.AnimatorListener() {
                    boolean cancelled;

                    @Override
                    public void onAnimationStart(Animator animation) {
                        cancelled = false;
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (!cancelled) {
                            setGrowing();
                            if (completeAnimOnNextCycle) {
                                completeAnimOnNextCycle = false;
                                completeAnim.start();
                            } else {
                                growAnim.start();
                            }
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        cancelled = true;
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                    }
                });
    }

    private void setupCompleteAnimation() {
        completeAnim = animationFactory.buildAnimation(ArcAnimationFactory.Type.COMPLETE,
                new ValueAnimator.AnimatorUpdateListener() {

                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float animatedFraction = AnimationUtils.getAnimatedFraction(animation);
                        float angle = minSweepAngle + animatedFraction * 360;
                        updateCurrentSweepAngle(angle);
                    }
                }, new Animator.AnimatorListener() {
                    boolean cancelled = false;

                    @Override
                    public void onAnimationStart(Animator animation) {
                        cancelled = false;
                        growing = true;
                        rotateAnim.setInterpolator(new DecelerateInterpolator());
                        rotateAnim.setDuration(ArcAnimationFactory.COMPLETE_ROTATE_DURATION);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (!cancelled) {
                            stop();
                        }
                        completeAnim.removeListener(this);
                        if (progressCompleteListener != null)
                            progressCompleteListener.animComplete();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        cancelled = true;
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                    }
                });
    }

    private void setupHideAnim() {
        fadeOutAnim = ObjectAnimator.ofFloat(this, "alpha", 1, 0);
        fadeOutAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                invalidate();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        fadeOutAnim.setDuration(200);
    }

    private void setupShowAnim() {
        this.setAlpha(0);
        fadeInAnim = ObjectAnimator.ofFloat(this, "alpha", 0, 1);
        fadeInAnim.setDuration(200);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        float startAngle = currentRotationAngle - currentRotationAngleOffset;
        float sweepAngle = currentSweepAngle;
        if (!growing) {
            startAngle = startAngle + (360 - sweepAngle);
        }

        canvas.drawArc(arcBounds, startAngle, sweepAngle, false, paint);
    }


    private void resetProperties() {
        currentSweepAngle = 0;
        currentRotationAngle = 0;
        currentRotationAngleOffset = 0;
    }

    private void setGrowing() {
        growing = true;
        currentRotationAngleOffset += minSweepAngle;
    }

    private void setShrinking() {
        growing = false;
        currentRotationAngleOffset = currentRotationAngleOffset + (360 - maxSweepAngle);
    }

    public void start() {
        resetProperties();
        rotateAnim.start();
        growAnim.start();
        invalidate();
        fadeInAnim.start();
    }

    public void startComplete() {
        boolean startNewComplete = !shrinkAnim.isStarted() && !growAnim.isStarted();
        if (startNewComplete) {
            completeAnim.start();
            fadeInAnim.start();
        }
        completeAnimOnNextCycle = true;
    }

    public void stop() {
        stopAnimators();
        fadeOutAnim.start();
    }

    public void reset() {
        resetProperties();
        stop();
    }

    public void startProgress() {
        resetProperties();
        stopAnimators();
        invalidate();

        this.currentRotationAngle = -90;
        this.growing = true;
    }

    public void setProgress(int progress) {
        if (!progressAnim.isStarted()) {
            int angle = progress * 360 / maxProgress;
            progressAnim.setFloatValues(currentSweepAngle, angle);
            currentSweepAngle = angle;
            progressAnim.start();
        }
    }

    public void setMaxProgress(int maxProgress) {
        this.maxProgress = maxProgress;
    }

    public void setProgressCompleteListener(ProgressCompleteListener progressCompleteListener) {
        this.progressCompleteListener = progressCompleteListener;
    }

    private void stopAnimators() {
        rotateAnim.cancel();
        growAnim.cancel();
        shrinkAnim.cancel();
        completeAnim.cancel();
    }

    void updateCurrentRotationAngle(float currentRotationAngle) {
        this.currentRotationAngle = currentRotationAngle;
        invalidate();
    }

    void updateCurrentSweepAngle(float currentSweepAngle) {
        this.currentSweepAngle = currentSweepAngle;
        invalidate();
    }
}